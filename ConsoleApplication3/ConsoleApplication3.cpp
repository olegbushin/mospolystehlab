// ConsoleApplication3.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include <iostream>
#include <bitset>
// � �������� ������� ����� ������, ������� � ������ ������� ������� ������ �����������, ������� � ����� ������ �������
// � ������� ������� ����� ������ ������ � ����������� ������� ��������� �����


// ��������� �� ����� ��������, ��������� ����������� ����� ��������.
// ������������� ����� ������������ ����� ��� ���������� �������� � �������������� ����.
// ��������(���������),��������������(+1)

// /* */ - ��� ����������� ����� C++. ����� ������ ��� , ��� ������, ���� ����� ������������ ������������


// 1) �������� (��� ����� "�������" ����� � �����)


// ������ �������� ����� ��������� ���� � ����������� *.c/*.cpp � ������ ���������� *.h/*.hpp (header)
// ��� ��������� ���� ���������� �������� ���������� ����� ��������� ������ ��� include


// 2)������� � ������ main ����������� �������� ������ ����� (entry point) � ���������� 
// �������  - ��� �������� ����, ������� ����������� ����������� - ��� �������
// ��������� ��������� ���������� �������� ���������� ������ �� ������� main �� ���� �����������
int main()
{
	/*����������� ��� ������� �����, �� ������ ������� �� return */

	int a; // ���������� ������������� ���������� �� ������
	a = -10; // ������������������� ( ��������� ��������� ��������)
	std::cout << "a_dec = \n " << a << '\n'; // 1)����� � ������� 2) ������� ����������� ������������� ����� 3) ������� ����������� ������
	std::cout << "a_bin = " << std::bitset<32>(a) << std::endl;

	// std::endl ���������� �������� ������ �������� ������ "\n"

	a *= -1; // ������������  ������ a = a * -1
	std::cout << "a_bin = " << std::bitset<32>(a) << std::endl;

	std::cout << "sizeof(int) = " << sizeof(int) << std::endl;
	std::cout << "sizeof(char) = " << sizeof(char) << std::endl;
	std::cout << "sizeof(wchar_t) = " << sizeof(wchar_t) << std::endl;
	std::cout << "sizeof(bool) = " << sizeof(bool) << std::endl;
	std::cout << "sizeof(float) = " << sizeof(float) << std::endl;
	std::cout << "sizeof(double) = " << sizeof(double) << std::endl;
	std::cout << "sizeof(shot) = " << sizeof(short) << std::endl;
	std::cout << "sizeof(long) = " << sizeof(long) << std::endl;
	std::cout << "sizeof(signed) = " << sizeof(signed) << std::endl;
	std::cout << "sizeof(unsigned) = " << sizeof(unsigned) << std::endl;
	bool b = true;
	std::cout << "bool b = " << std::bitset<8>(b) << std::endl;
	b = false;
	std::cout << "bool b = " << std::bitset<8>(b) << std::endl;

	short s = 1000;
	std::cout << "short s = " << std::bitset<16>(s) << std::endl;
	s *= -1;
	std::cout << "short s = " << std::bitset<16>(s) << std::endl;
	 

	unsigned int c = 10;
	std::cout << "unsigned int c = " << std::bitset<32>(c) << std::endl;
	c *= -1;
	std::cout << "unsigned int c = " << std::bitset<32>(c) << std::endl;
	std::cout << "unsigned int c = " << c << std::endl;
	c = 0b11111111111111111111111111111111; // �������� ������ ����� - ����� 0b
	std::cout << "max unsigned int = " << c << std::endl; // max = 4294967295

	a = 0b01111111111111111111111111111111;
	std::cout << "max int = " << a << std::endl; // max int = 2147483647
	a = 0b10000000000000000000000000000000;
	std::cout << "max int = " << a << std::endl; // max int = -2147483647




	getchar(); // 1) ������� 2) �������� ����� ����� �������� 1 �������

	return 0; // 1) ����� ���������� ������� 2) ���������� ������� main �������� ������ ���������� ������ ����������

			  // ��, ��� ����� ��������� �����, ��������������, �� ������� �� ����� �����������.
}